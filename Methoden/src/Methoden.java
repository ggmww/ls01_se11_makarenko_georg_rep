import java.util.Scanner;
public class Methoden {
static Scanner sc = new Scanner(System.in);

public static void main(String[] args) {
	double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;


//1.Programmhinweis
hinweis();

//4.Eingabe
eingabe(zahl1, zahl2);

//3.Verarbeitung
erg = verarbeitung(zahl1, zahl2);

//2.Ausgabe 
ausgabe(zahl1, zahl2, erg);

}
public static void hinweis() {
System.out.println("Hinweis: ");
System.out.println("Das Programm addiert 2 eingegebene Zahlen. "); 

}

public static void ausgabe(double zahl1, double zahl2, double erg) {
	System.out.println("Ergebnis der Addition");
	System.out.printf("%.2f + %.2f = %.2f", zahl1, zahl2, erg);
}

public static double verarbeitung(double zahl1, double zahl2) {
double erg;
	erg = zahl1 + zahl2;	
	return erg;
}

public static void  eingabe(double zahl1, double zahl2) {
System.out.println(" 1. Zahl:");
zahl1 = sc.nextDouble();
System.out.println(" 2. Zahl:");
zahl2 = sc.nextDouble();

}

}