import java.util.Scanner;
public class Aufgabe4 {
	public static void main(String[] args)
	 {
	 Scanner sc= new Scanner(System.in);
	 int x;
	 
	 System.out.print("Bitte geben Sie eine Zahl von 0 bis 100 ein:");
	 x = sc.nextInt();
	 if  ((x < 0) || (x > 100))  {
		 
			System.out.print("Bitte geben Sie eine Zahl NUR von 0 bis 100 ein!");
			}
	 else if (x > 50) {
		 System.out.print(x + " ist grosse Zahl");
	 }
	 else {
		 System.out.print(x + " ist kleine Zahl");
	 }
	 
	 sc.close();
	 }
	}